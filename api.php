<?php
    session_start();
    include("include/config.inc.php");
    include("include/sets.inc.php");
    include("include/users.inc.php");

    switch($_GET['action'])
    {
        case "getUserInfo":
            echo json_encode($_SESSION);
            break;

        case "login":
            if(($result = userLogin($pdo, $_GET['login'], $_GET['password'])))
            {
                $_SESSION['loggedIn'] = true;
                $_SESSION['userId'] = $result['id'];
                $_SESSION['displayName'] = $result['displayname'];
                $_SESSION['permissions'] = userGetPermissionList($pdo, $_SESSION['userId']);
                echo json_encode(array("status" => "success"));
            }
            else {
                echo json_encode(array("status" => "invalid"));
            }
            break;

        case "logout":
            echo json_encode(array("was_logged" => $_SESSION['loggedin']));
            session_destroy();
            break;

        case "changePassword":
            userChangePassword($pdo, $_GET['userId'], $_GET['newPassword']);
            break;

		case "getSetList":
			echo json_encode(isset($_GET['userId']) ? setGetListByUserId($pdo, $_SESSION['userId']) : setGetList($pdo));
            break;

        case "addSet":
            if(!isset($_SESSION['userId']))
            {
                echo json_encode(array("status" => "not_logged_in"));
                break;
            }

            if(!isset($_GET['name']))
            {
                echo json_encode(array("status" => "invalid_name"));
                break;
            }
            try {
                $id = setAdd($pdo, $_SESSION['userId'], $_GET['name']);
                echo json_encode(array("status" => "success", "setId" => $id));
            } catch (PDOException $e) {
                if($e->getCode() == 23000)
                {
                    echo json_encode(array("status" => "name_already_used"));
                }
                else {
                    echo json_encode(array("status" => "unknown_error", "errormsg" => $e->getMessage()));
                }
            }
            break;

        case "getSetComponents":
            if(!isset($_GET['setId']) || $_GET['setId'] <= 0)
            {
                echo json_encode(array("status" => "invalid_id"));
                break;
            }

            echo json_encode(setGetComponents($pdo, $_GET['setId']));
            break;

        case "getComponents":
            echo json_encode(componentsList($pdo));
            break;

        case "getComponentsByType":
            echo json_encode(componentGetByType($pdo, $_GET['type']));
            break;

        case "getComponent":
            if(!isset($_GET['componentId']) || $_GET['componentId'] <= 0)
                break;

            echo json_encode(componentGetById($pdo, $_GET['componentId']));
            break;

        case "getComponentSpecs":
            if(!isset($_GET['componentId']) || $_GET['componentId'] <= 0)
                break;

            echo json_encode(componentSpecsList($pdo, $_GET['componentId']));
            break;

        case "addComponentToSet":
            if(!isset($_GET['setId']) || !isset($_GET['componentId']))
                break;

            try
            {
                setAddComponent($pdo, $_GET['setId'], $_GET['componentId']);
                echo json_encode(array("status" => "success"));
            }
            catch(PDOException $e)
            {
                echo json_encode(array("status" => "error", "errormsg" => $e->getMessage()));
            }
            break;

        case "removeComponentFromSet":
            if(!isset($_GET['setId']) || !isset($_GET['componentId']))
                break;

            try {
                setDeleteComponent($pdo, $_GET['setId'], $_GET['componentId']);
                echo json_encode(array("status" => "success"));
            } catch (PDOException $e) {
                echo json_encode(array("status" => "error", "errormsg" => $e->getMessage()));
            }

            break;

        case "changeComponentAmount":
            if(!isset($_GET['setId']) || !isset($_GET['componentId']) || !isset($_GET['amount']) || $_GET['amount'] <= 0)
                break;

            try {
                setChangeComponentAmount($pdo, $_GET['setId'], $_GET['componentId'], $_GET['amount']);
                echo json_encode(array("status" => "success"));
            } catch (PDOException $e) {
                echo json_encode(array("status" => "error", "errormsg" => $e->getMessage()));
            }
            break;

        case "getUserOrders":
            if(!isset($_SESSION['loggedIn']))
            {
                echo json_encode(array("status" => "not_logged_in"));
                break;
            }

            echo json_encode(array("status" => "success", "data" => orderGetListByUserId($pdo, $_SESSION['userId'])));
            break;

        case "orderSet":
            $components = setGetComponents($pdo, $_GET['setId']);
            $insufficient = array();
            foreach($components as &$row)
            {
                if($row['amount'] > $row['totalAmount'])
                {
                    $insufficient[] = $row['id'];
                }
            }

            if(!empty($insufficient))
            {
                echo json_encode(array("status" => "insufficient_items", "ids" => $insufficient));
                break;
            }

            orderAdd($pdo, $_SESSION['userId'], $_GET['setId']);

            foreach($components as &$row)
            {
                componentChange($pdo, $row['id'], array("amount" => "amount - ".$row['amount']));
            }
            echo json_encode(array("status" => "success"));
            break;
    }
?>
