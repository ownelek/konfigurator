<?php
	$host = "127.0.0.1";
	$user = "root";
	$pass = "abc123";
	$db = "konfigurator";

	try
	{
		$pdo = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass", array(
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_PERSISTENT => true
		));

        $pdo->exec("SET NAMES utf8");
	}
	catch(PDOException $e)
	{
		echo $e;
	}
?>
