<?php
function userGetList($pdo)
{
    return $pdo->query("SELECT * FROM pk_users")->fetchAll();
}

function userGetById($pdo, $userId)
{
    $q = $pdo->prepare("SELECT * FROM pk_users WHERE id = ?");
    $q->execute(array($userId));
    return $q->fetchAll();
}

function userGetNameLookup($pdo, $name)
{
    $q = $pdo->prepare("SELECT * FROM pk_users WHERE displayname LIKE ?");
    $q->execute(array(implode("", array("%", $name, "%"))));
    return $q->fetchAll();
}

function userGetLoginLookup($pdo, $login)
{
    $q = $pdo->prepare("SELECT * FROM pk_users WHERE login LIKE ?");
    $q->execute(array(implode("", array("%", $login, "%"))));
    return $q->fetchAll();
}

function userGetPermissionList($pdo, $userId)
{
    $q = $pdo->prepare("
        SELECT p.permission_name, gp.negate
        FROM pk_groups_permissions AS gp
        INNER JOIN pk_groups_users AS gu ON gu.group_id = gp.group_id
        INNER JOIN pk_permissions AS p ON p.id = gp.permission_id
        WHERE gu.user_id = ?
        ORDER BY gu.priority DESC
    ");
    $q->execute(array($userId));
    return permissionsEvaluate($q->fetchAll());
}

function userRegister($pdo, $login, $displayName, $password, $ip, $ipv6)
{
	$q = $pdo->prepare("INSERT INTO pk_users (login, displayname, password, ip, ipv6) VALUES(?, ?, ?, ?, ?)");
	$q->execute(array($login, $displayName, password_hash($password, PASSWORD_DEFAULT), $ip, $ipv6));
    return $pdo->lastInsertId();
}

function userLogin($pdo, $login, $password)
{
	$q = $pdo->prepare("SELECT * FROM pk_users WHERE login = ?");
	$q->execute(array($login));
    if($q->rowCount() < 1)
        return false;

	$result = $q->fetch();
	if(password_verify($password, $result['password']))
		return $result;

	return false;
}

function userChangePassword($pdo, $userId, $new_password)
{
    $q = $pdo->prepare("UPDATE pk_users SET password = ? WHERE id = ?");
    $q->execute(array(password_hash($new_password, PASSWORD_DEFAULT), $userId));
    return $q->rowCount() > 0;
}

function userDelete($pdo, $userId)
{
    $q = $pdo->prepare("DELETE FROM pk_users WHERE id = ?");
    $q->execute(array($userId));
}

function groupGetList($pdo)
{
    return $pdo->query("SELECT * FROM pk_groups")->fetchAll();
}

function groupAdd($pdo, $name)
{
    $q = $pdo->prepare("INSERT INTO pk_groups (name) VALUES(?)");
    $q->execute(array($name));
    return $pdo->lastInsertId();
}

function groupRename($pdo, $groupId, $name)
{
    $q = $pdo->prepare("UPDATE pk_groups SET name = ? WHERE id = ?");
    $q->execute(array($name, $groupId));
}

function groupDelete($pdo, $groupId)
{
    $q = $pdo->prepare("DELETE FROM pk_groups WHERE id = ?");
    $q->execute(array($groupId));
}

function groupGetUserList($pdo, $groupId)
{
    $q = $pdo->prepare("SELECT u.* FROM pk_groups_users AS g INNER JOIN pk_users AS u ON g.user_id = u.id WHERE g.group_id = ?");
    $q->execute(array($groupId));
    return $q->fetchAll();
}

function groupAddUser($pdo, $groupId, $userId)
{
    $pdo->prepare("INSERT INTO pk_groups_users (group_id, user_id) VALUES(?, ?)")->execute(array($groupId, $userId));
}

function groupDeleteUser($pdo, $groupId, $userId)
{
    $pdo->prepare("DELETE FROM pk_groups_users WHERE group_id = ? AND user_id = ?")->execute(array($groupId, $userId));
}

function groupGetPermissions($pdo, $groupId)
{
    $q = $pdo->prepare("SELECT p.permission_name, g.negate FROM pk_groups_permissions AS g INNER JOIN pk_permissions AS p ON g.permissions_id = p.id WHERE g.group_id = ?");
    $q->execute(array($groupId));
    return $q->fetchAll();
}

function groupAddPermission($pdo, $groupId, $permissionId)
{
    $pdo->prepare("INSERT INTO pk_groups_permissions (group_id, permission_id) VALUES(?, ?)")->execute(array($groupId, $permissionId));
}

function groupDeletePermission($pdo, $groupId, $permissionId)
{
    $pdo->prepare("DELETE FROM pk_groups_permissions WHERE group_id = ? AND permission_id = ?")->execute(array($groupId, $permissionId));
}

function permissionList($pdo)
{
    return $pdo->query("SELECT * FROM pk_permissions")->fetchAll();
}

function permissionAdd($pdo, $name)
{
    $pdo->prepare("INSERT INTO pk_permissions (name) VALUES(?)")->execute(array($name));
}

function permissionDelete($pdo, $permissionId)
{
    $pdo->prepare("DELETE FROM pk_permissions WHERE id = ?")->execute(array($permissionId));
}

function permissionsEvaluate($permissions)
{
    $return = array();
    foreach($permissions as $permission)
    {
        if($permission['negate'])
        {
            $index = array_search($return, $permission['permission_name']);

            if($index !== false)
            {
                array_splice($return, $index, 1);
            }
        }
        else {
            $return[] = $permission['permission_name'];
        }
    }

    return $return;
}
?>
