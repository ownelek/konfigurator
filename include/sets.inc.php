<?php
    function componentsList($pdo)
    {
        return $pdo->query("SELECT * FROM pk_components")->fetchAll();
    }

    function componentGetById($pdo, $componentId)
    {
        $q = $pdo->prepare("SELECT * FROM pk_components WHERE id = ?");
        $q->execute(array($componentId));
        return $q->fetch();
    }

    function componentNameLookup($pdo, $search)
    {
        $q = $pdo->prepare("SELECT * FROM pk_components WHERE name LIKE ?");
        $q->execute(array(implode("", array("%", $search, "%"))));
        return $q->fetchAll();
    }

    function componentGetByType($pdo, $type)
    {
        $q = $pdo->prepare("SELECT * FROM pk_components WHERE type = ?");
        $q->execute(array($type));
        return $q->fetchAll();
    }

	function componentAdd($pdo, $name, $type, $picturePath, $amount, $price)
	{
		$q = $pdo->prepare("INSERT INTO pk_components (name, type, picture, amount, price) VALUES(?, ?, ?, ?, ?)");
		$q->execute(array($name, $type, $picturePath, $amount, $price));
	}

	function componentChange($pdo, $id, $columns)
	{
		$pairs = array();
		foreach($columns as $key => $value)
		{
			$pairs[] = $key."=".$value;
		}
		$sql = "UPDATE pk_components SET ".implode(",", $pairs)." WHERE id = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($id));
	}

	function componentDelete($pdo, $id)
	{
		$q = $pdo->prepare("DELETE FROM pk_components WHERE id = ?");
		$q->execute(array($id));
	}

    function componentSpecsList($pdo, $componentId)
    {
        $q = $pdo->prepare("SELECT * FROM pk_components_specs WHERE component_id = ?");
        $q->execute(array($componentId));
        return $q->fetchAll();
    }

	function componentSpecsSet($pdo, $componentId, $specs)
	{
		$q = $pdo->prepare("REPLACE INTO pk_components_specs (component_id, key, value) VALUES(?, ?, ?)");
		foreach($specs as $key => $value)
		{
			$q->execute(array($componentId, $key, $value));
		}
	}

	function componentSpecAdd($pdo, $componentId, $key, $value)
	{
        $q = $pdo->prepare("INSERT INTO pk_components_specs (component_id, key, value) VALUES(?, ?, ?)");
        $q->execute(array($componentId, $key, $value));
	}

	function componentSpecDelete($pdo, $specId)
	{
        $q = $pdo->prepare("DELETE FROM pk_components_specs WHERE id = ?");
        $q->execute(array($specId));
	}

    function setGetList($pdo)
    {
        return $pdo->query("SELECT * FROM pk_sets")->fetchAll();
    }

    function setGetListByUserId($pdo, $userId)
    {
        $q = $pdo->prepare("SELECT * FROM pk_sets WHERE user_id = ?");
        $q->execute(array($userId));
        return $q->fetchAll();
    }

    function setGetListNameLookup($pdo, $name)
    {
        $q = $pdo->prepare("SELECT * FROM pk_sets WHERE name LIKE ?");
        $q->execute(array(implode("", array("%", $name, "%"))));
        return $q->fetchAll();
    }

    function setAdd($pdo, $userId, $name)
    {
        $q = $pdo->prepare("INSERT INTO pk_sets (user_id, name) VALUES(?, ?)");
        $q->execute(array($userId, $name));
        return $pdo->lastInsertId();
    }

    function setDelete($pdo, $setId)
    {
        $q = $pdo->prepare("DELETE FROM pk_sets WHERE id = ?");
        $q->execute(array($setId));
    }

    function setGetComponents($pdo, $setId)
    {
        $q = $pdo->prepare("SELECT c.id, c.name, c.type, c.picture, c.amount AS totalAmount, s.amount, c.price FROM pk_sets_components AS s, pk_components AS c WHERE s.component_id = c.id AND s.set_id = ?");
        $q->execute(array($setId));
        return $q->fetchAll();
    }

    function setAddComponent($pdo, $setId, $componentId)
    {
        $q = $pdo->prepare("INSERT INTO pk_sets_components (set_id, component_id) VALUES(?, ?) ON DUPLICATE KEY UPDATE amount = amount + 1");
        $q->execute(array($setId, $componentId));
    }

    function setChangeComponentAmount($pdo, $setId, $componentId, $amount)
    {
        $q = $pdo->prepare("UPDATE pk_sets_components SET amount = ? WHERE set_id = ? AND component_id = ?");
        $q->execute(array($amount, $setId, $componentId));
    }

    function setDeleteComponent($pdo, $setId, $componentId)
    {
        $q = $pdo->prepare("DELETE FROM pk_sets_components WHERE set_id = ? AND component_id = ?");
        $q->execute(array($setId, $componentId));
    }

    function orderGetList($pdo)
    {
        return $pdo->query("
            SELECT pk_orders.id, pk_users.displayname, pk_sets.name, pk_orders.date, pk_orders.status FROM pk_orders
            INNER JOIN pk_sets ON pk_orders.set_id = pk_sets.id
            INNER JOIN pk_users ON pk_orders.user_id = pk_users.id
        ")->fetchAll();
    }

    function orderGetListByUserId($pdo, $userId)
    {
        $q = $pdo->prepare("
            SELECT pk_orders.id, pk_sets.name, pk_orders.date, pk_orders.status, pk_orders.price FROM pk_orders
            INNER JOIN pk_sets ON pk_orders.set_id = pk_sets.id
            WHERE pk_orders.user_id = ?
        ");
        $q->execute(array($userId));
        return $q->fetchAll();
    }

    function orderGetListBySetId($pdo, $setId)
    {
        $q = $pdo->prepare("SELECT * FROM pk_orders WHERE set_id = ?");
        $q->execute(array($setId));
        return $q->fetchAll();
    }

    function orderGetListByUserIdSetId($pdo, $userId, $setId)
    {
        $q = $pdo->prepare("SELECT * FROM pk_orders WHERE user_id = ? AND set_id = ?");
        $q->execute(array($userId, $setId));
        return $q->fetchAll();
    }

    function orderAdd($pdo, $userId, $setId)
    {
        $q = $pdo->prepare("SELECT SUM(c.price) AS price FROM pk_components AS c INNER JOIN pk_sets_components AS sc ON sc.component_id = c.id WHERE sc.set_id = ?");
        $q->execute(array($setId));

        $result = $q->fetch();

        $q = $pdo->prepare("INSERT INTO pk_orders (user_id, set_id, price) VALUES(?, ?, ?)");
        $q->execute(array($userId, $setId, $result['price']));
        return $pdo->lastInsertId();
    }

    function orderUpdate($pdo, $orderId, $status)
    {
        $q = $pdo->prepare("UPDATE pk_orders SET status = ? WHERE id = ?");
        $q->execute(array($status, $orderId));

        if($status == "accepted")
        {
            $q = $pdo->prepare("
                SELECT c.id FROM pk_components AS c
                INNER JOIN pk_sets_components AS sc ON sc.component_id = c.id
                INNER JOIN pk_orders AS o ON sc.set_id = o.set_id
                WHERE o.id = ?
            ");
            $q->execute(array($orderId));
            $ids = $q->fetchAll();

            $q = $pdo->prepare("UPDATE pk_components SET amount = amount - 1 WHERE id = ?");
            foreach($ids as $id)
            {
                $q->execute(array($id['id']));
            }
        }
    }

    function orderCancel($pdo, $orderId)
    {
        $q = $pdo->prepare("SELECT * FROM pk_orders WHERE id = ?");
        $q->execute(array($orderId));
        if($q->rowCount() < 1)
            return false;

        $result = $q->fetch();
        if($result['status'] != 'submitted')
            return false;

        $q = $pdo->prepare("UPDATE pk_orders SET status = 'canceled' WHERE id = ?");
        $q->execute(array($orderId));
        return true;
    }
?>
